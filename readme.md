# Telegram Chat Statistics
A simple program to see cool statistics of a long-running chat history with a friend.

## Usage
just dump a result.json to the project folder (rename to telegram.json) and `npm start` to see cool statistics.
Now also supports whatsapp history as well! Dump a whatsapp.txt to the root folder to add that too.
