const fs = require('fs')

function addDays(date, days) {
    const newDate = new Date(date.valueOf())
    newDate.setDate(newDate.getDate() + days)
    return newDate
}

let lastDay = new Date()
let missedDays = 0
/** @type {Record<string, [number, number]>} */
const userMessageCounts = {}
let totalLength = 0;
let totalMessages = 0
let firstDate = new Date()
let lastDate = new Date()
const providers = {}
const words = {}

function updateProviderStats(provider, total, totalLength) {
    if (providers[provider]) {
        providers[provider] = {
            total: providers[provider].total + total,
            totalLength: providers[provider].totalLength + totalLength,
        }
    } else {
        providers[provider] = {
            total: total,
            totalLength: totalLength,
        }
    }
}
function updateWordsFromText(text) {
    if (text) {
        if (Array.isArray(text)) {
            text = text.join('')
        }
        const wordsInText = text.split(' ');
        for (const wordRaw of wordsInText) {
            const word = wordRaw
                .toLowerCase()
                .replace(/^[^a-z]|[^a-z]$/g, '');
            if (word.length > 0) {
                if (words[word]) {
                    words[word]++
                } else {
                    words[word] = 1
                }
            }
        }
    }
}

function telegram(fileName = 'telegram.json') {
    if (fs.existsSync(fileName)) {
        console.log('Telegram file: %s', fileName)
    } else {
        return
    }
    updateProviderStats('Telegram', 0, 0)
    const file = fs.readFileSync(fileName)
    const content = JSON.parse(String(file))
    const { name, id, messages } = content;

    const _firstDate = new Date(messages[0].date)
    const _lastDate = new Date(messages[messages.length - 1].date)
    firstDate = _firstDate < firstDate ? _firstDate : firstDate
    lastDate = _lastDate > lastDate ? _lastDate : lastDate

    for (const message of messages) {
        const date = new Date(message.date);
        const currDay = date.getDate()
        const prevDay = addDays(date, -1).getDate()
    
        if (message.from) {
            const textLength = message.text.length;
            totalLength += textLength;
            userMessageCounts[message.from] = userMessageCounts[message.from] ? [
                userMessageCounts[message.from][0] + 1,
                userMessageCounts[message.from][1] + textLength
            ] : [
                1,
                textLength
            ]
            updateProviderStats('Telegram', 1, textLength)
            totalMessages++
        }
    
        if (![ currDay, prevDay ].includes(lastDay)) {
            missedDays++
        }
    
        lastDay = currDay;

        updateWordsFromText(message.text)
    }
}

function whatsapp(fileName = 'whatsapp.txt') {

    function parseMessage(str) {
        const [match, dateString, sender, message] = /(.*?)\s-\s(.*?):\s(.*)/.exec(str) || []
        if (!match) { return null }
        const [dateMatch, month, day, year, hour, minute] = /(\d{1,2})\/(\d{1,2})\/(\d{1,2}),\s(\d{1,2}):(\d{1,2})/.exec(dateString) || []
        const date = new Date(
            Number.parseInt(year) + 2000,
            Number.parseInt(month) - 1,
            day,
            hour,
            minute
        )
        if (!dateMatch) { return null }
        return {
            date,
            from: sender,
            text: message === '<Media omitted>' ? '' : message,
        }
    }

    if (fs.existsSync(fileName)) {
        console.log('WhatsApp file: %s', fileName)
    } else {
        return
    }
    updateProviderStats('WhatsApp', 0, 0)
    const file = fs.readFileSync(fileName)
    const lines = String(file).split('\n')
    const messages = lines.map(line => parseMessage(line)).filter(m => !!m)

    const _firstDate = messages[0].date
    const _lastDate = messages[messages.length - 1].date
    firstDate = _firstDate < firstDate ? _firstDate : firstDate
    lastDate = _lastDate > lastDate ? _lastDate : lastDate

    for (const message of messages) {
        const date = message.date;
        const currDay = date.getDate()
        const prevDay = addDays(date, -1).getDate()
    
        if (message.from) {
            const textLength = message.text.length;
            totalLength += textLength;
            userMessageCounts[message.from] = userMessageCounts[message.from] ? [
                userMessageCounts[message.from][0] + 1,
                userMessageCounts[message.from][1] + textLength
            ] : [
                1,
                textLength
            ]
            updateProviderStats('WhatsApp', 1, textLength)
            totalMessages++
        }
    
        if (![ currDay, prevDay ].includes(lastDay)) {
            // console.log('now %d prev %d last %d', currDay, prevDay, lastDay)
            missedDays++
        }
    
        lastDay = currDay;

        updateWordsFromText(message.text)
    }
}


telegram()
whatsapp()

const spanDays = (lastDate.valueOf() - firstDate.valueOf()) / 1000 / 60 / 60 / 24
const spanYears = spanDays / 365
const spanPercentage = (Math.ceil(spanDays) - missedDays) / spanDays

console.log('Messages: %s', totalMessages)
console.log('Messages span over %s days (%s years)', spanDays.toFixed(0), spanYears.toFixed(2))
console.log('Of those days, %s were days when messages were exchanged', Math.ceil(spanDays) - missedDays)
console.log('This means %d missed days, with a percentage of %s%', missedDays, (spanPercentage * 100).toFixed(2))
console.log('People with messages sent:')
for (const person in userMessageCounts) {
    const messageCount = userMessageCounts[person][0]
    const messageTotalLength = userMessageCounts[person][1]
    const messageCountPercentage = messageCount / totalMessages
    const messageTotalLengthPercentage = messageTotalLength / totalLength;

    console.log(' %s%s%s messages (%s% of messages, %s% of text)', person, ' '.repeat(30 - person.length), messageCount, (messageCountPercentage * 100).toFixed(2), (messageTotalLengthPercentage * 100).toFixed(2))
}

console.log('Messaging services:')
for (const provider in providers) {
    const messageCount = providers[provider].total
    const messageTotalLength = providers[provider].totalLength
    const messageCountPercentage = messageCount / totalMessages
    const messageTotalLengthPercentage = messageTotalLength / totalLength;

    console.log(' %s%s%s messages (%s% of messages, %s% of text)', provider, ' '.repeat(30 - provider.length), messageCount, (messageCountPercentage * 100).toFixed(2), (messageTotalLengthPercentage * 100).toFixed(2))
}

console.log('Most used words:')
const sortedWords = Object.keys(words)
    .sort((a, b) => words[b] - words[a])
const topWords1 = sortedWords
    .slice(0, 20)
const topWords3 = sortedWords
    .filter(w => w.length >= 3)
    .slice(0, 20)
console.log('minimum length 1:', topWords1.map(a => `${a} (${words[a]})`).join(', '))
console.log('minimum length 3:', topWords3.map(a => `${a} (${words[a]})`).join(', '))